<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinjamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');  
            $table->unsignedBigInteger('buku_id');  
            $table->date('tgl_pinjam');  
            $table->date('tgl_bataspinjam');  
            $table->date('tgl_kembali');  
            $table->boolean('status');
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');      
            $table->foreign('buku_id')->references('id')->on('buku'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjams');
    }
}
