<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama',50);
            $table->string('nim',20);
            $table->unsignedBigInteger('user_id');    
            $table->unsignedBigInteger('fakultas_id');    
            $table->unsignedBigInteger('jurusan_id');    
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');                       
            $table->foreign('fakultas_id')->references('id')->on('fakultas'); 
            $table->foreign('jurusan_id')->references('id')->on('jurusan'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
