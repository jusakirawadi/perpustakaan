<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pinjam extends Model
{
    protected $table = 'pinjams';
    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function buku() {
        return $this->belongsTo('App\Models\Buku','buku_id');
    }
}
