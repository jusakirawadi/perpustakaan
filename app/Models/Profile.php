<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function fakultas() {
        return $this->belongsTo('App\Models\Fakultas','fakultas_id');
    }

    public function jurusan() {
        return $this->belongsTo('App\Models\Jurusan','jurusan_id');
    }
}
