<?php

namespace App\Http\Controllers\Perpus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\BukuResource;
use App\Models\Buku;

class BukuController extends Controller
{
 
    public function index()
    {
        $buku = Buku::all();
        return BukuResource::collection($buku);
    }

    public function store(Request $request)
    {
        $request->validate([
            'id' => ['required'],
            'judul' => ['required'],
            'pengarang' => ['required'],
            'tahun_terbit' => ['required'],
        ]);
 
       
        $buku = Buku::create([
             'id' => request('id'),
             'judul' => request('judul'),
             'pengarang' => request('pengarang'),
             'tahun_terbit' => request('tahun_terbit')
        ]);
 
        return $buku;
    }

    public function show($id)
    {
        $buku = Buku::where('id', $id)->first();
        return new BukuResource($buku);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'id' => ['required'],
            'judul' => ['required'],
            'pengarang' => ['required'],
            'tahun_terbit' => ['required'],
        ]);
 
       
        $result = Buku::where('id',$id)->update([
             'id' => request('id'),
             'judul' => request('judul'),
             'pengarang' => request('pengarang'),
             'tahun_terbit' => request('tahun_terbit')
        ]);
 
        return $result;
    }

    public function destroy($id)
    {
        Buku::where('id', $id)->delete();
        return response()->json('Buku sudah dihapus !',200);
    }
}
