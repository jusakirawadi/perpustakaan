<?php

namespace App\Http\Controllers\Perpus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ProfileResource;
use App\Models\Profile;

class ProfileController extends Controller
{
   
    public function index()
    {
        $profile = Profile::all();
        return ProfileResource::collection($profile);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'nim' => ['required'],
            'fakultas_id' => ['required'],
            'jurusan_id' => ['required'],
        ]);
 
       
        $profile = Auth()->user()->profile()->create([
             'nama' => request('nama'),
             'nim' => request('nim'),
             'fakultas_id' => request('fakultas_id'),
             'jurusan_id' => request('jurusan_id'),
        ]);
 
        return $profile;
    }

    public function show($id)
    {
        $profile = Profile::where('id', $id)->first();
        return new ProfileResource($profile);
    }
 
    public function update(Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'nim' => ['required'],
            'fakultas_id' => ['required'],
            'jurusan_id' => ['required'],
        ]);

        $result = Auth()->user()->profile()->update([
            'nama' => request('nama'),
            'nim' => request('nim'),
            'fakultas_id' => request('fakultas_id'),
            'jurusan_id' => request('jurusan_id'),
           ]); 

        if ($result==1) {
            return response()->json('Profile Sudah Diedit !',200);
        }
        return response()->json('Profile Tidak Ditemukan !',404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profile::where('id', $id)->delete();
        return response()->json('Profile Mahasiswa sudah dihapus !',200);
    }
}
