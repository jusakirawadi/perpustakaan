<?php

namespace App\Http\Controllers\Perpus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pinjam;
use App\Http\Resources\PinjamResource;

class PinjamController extends Controller
{
 
    public function index()
    {
        $pinjam = Pinjam::all();
        return PinjamResource::collection($pinjam);
    }

    public function index_user()
    {
        $pinjam = Auth::user()->pinjam;
        return PinjamResource::collection($pinjam);
    }


    public function store(Request $request)
    {
       $request->validate([
           'buku_id' => ['required'],
           'tgl_pinjam' => ['required'],
           'tgl_bataspinjam' => ['required'],
       ]);

      
       $pinjam = Auth()->user()->pinjam()->create([
            'buku_id' => request('buku_id'),
            'tgl_pinjam' => request('tgl_pinjam'),
            'tgl_bataspinjam' => request('tgl_bataspinjam'),
            'tgl_kembali' => request('tgl_kembali'),
            'status' => request('status'),
       ]);

       return $pinjam;
    }
   
    public function show($id)
    {
        $pinjam = Pinjam::where('buku_id', $id)->first();
        return new PinjamResource($pinjam);
    }
   
    public function update(Request $request, $id)
    {
       $request->validate([
            'tgl_kembali' => ['required'],
            'status' => ['required'],
        ]); 
        
        $result = Pinjam::where('id',$id)->update([
                        'tgl_kembali' => request('tgl_kembali'),
                        'status' => request('status'),
                    ]); 
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
