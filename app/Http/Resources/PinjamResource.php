<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PinjamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            "buku" => $this->buku->judul,
            "pengarang" => $this->buku->pengarang,
            "tahunterbit" => $this->buku->tahun_terbit,
            "peminjam" => $this->user->profile->nama,
            "nim" => $this->user->profile->nim,
            "fakultas" => $this->user->profile->fakultas->nama,
            "jurusan" => $this->user->profile->jurusan->nama,
            "tglpinjam" => $this->tgl_pinjam,
            "bataskembali" => $this->tgl_bataspinjam
        ];
    }
}
