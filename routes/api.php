<?php

/* 
    Penjelasan Route Auth
    Login   :   untuk login dan mendapatkan token
    Logout  :   untuk logout
    Register:   mendaftarkan user baru
 */

Route::namespace('Auth')->group(function() {
    Route::post('login','LoginController');
    Route::post('logout','LogoutController');
    Route::post('register','RegisterController');
});

/* 
    Penjelasan Route Perpus
    inputpinjam     :   user meminjam buku berdasarkan id-nya (id login)
    lihatpeminjam   :   melihat nama peminjam buku dan tgl. batas pinjam 
                        ( cari berdasar id buku )    
    daftarpeminjam  :   menampilkan semua daftar peminjam buku
    sayapinjam      :   menampilkan history buku yang user pinjam (sesuai id login)     
    daftarbuku      :   menampilkan semua daftar buku
    lihatbuku       :   menampilkan informasi buku
    inputprofil     :   mahasiswa dapat menginput informasi dirinya ( nim, nama lengkap, dsb)
    lihatprofil     :   menampilkan informasi seorang mahasiswa
    editprofil      :   mahasiswa dapat mengedit datanya
    
    Khusus ADMIN
    kembali         :   input pengembalian buku
    inputbuku       :   input buku baru
    editbuku        :   edit data buku
    hapusbuku       :   hapus buku
    daftarmhs       :   menampilkan daftar mahasiswa
    hapus  hs       :   hapus profile seorang mahasiswa
 */

Route::namespace('Perpus')->middleware('auth:api')->group(function() {
    Route::post('inputpinjam','PinjamController@store');   
    Route::get('lihatpeminjam/{id}','PinjamController@show');       
    Route::get('daftarpeminjam','PinjamController@index');       
    Route::get('sayapinjam','PinjamController@index_user');       

    Route::get('daftarbuku','BukuController@index');   
    Route::get('lihatbuku/{id}','BukuController@show');   

    Route::post('inputprofil','ProfileController@store');   
    Route::get('lihatprofil/{id}','ProfileController@show');   
    Route::patch('editprofil','ProfileController@update');   
});

Route::namespace('Perpus')->middleware(['auth:api','admin'])->group(function() {
    Route::patch('kembali/{id}','PinjamController@update');
    
    Route::post('inputbuku','BukuController@store'); 
    Route::patch('editbuku/{id}','BukuController@update');
    Route::delete('hapusbuku/{id}','BukuController@destroy');

    Route::get('daftarmhs','ProfileController@index');   
    Route::get('hapusmhs/{id}','ProfileController@destroy');   
});

